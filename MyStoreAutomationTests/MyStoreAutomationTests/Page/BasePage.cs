﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace MyStoreAutomationTests.Page
{
    public class BasePage
    {
        private IWebDriver driver;
        protected IWebElement PageRoot => driver.FindElement(By.Id("page"));
        protected WebDriverWait Wait;
        public BasePage(IWebDriver driver)
        {
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            this.driver = driver;
        }
    }
}
