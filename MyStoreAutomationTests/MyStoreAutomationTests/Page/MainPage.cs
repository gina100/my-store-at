﻿using OpenQA.Selenium;

namespace MyStoreAutomationTests.Page
{
    public class MainPage : BasePage
    {
        public MainPage(IWebDriver driver) :base(driver)
        {

        }

        private IWebElement searchBox => PageRoot.FindElement(By.Id("searchbox"));

        public IWebElement searchInput => searchBox.FindElement(By.Id("search_query_top"));

        public IWebElement searchButton => searchBox.FindElement(By.CssSelector("button"));

        public IWebElement contactUsButton => PageRoot.FindElement(By.CssSelector("#contact-link > a"));
    }
}
