﻿using MyStoreAutomationTests.Page;
using OpenQA.Selenium;

namespace MyStoreAutomationTests
{
    public static class StoreApp
    {
        public static MainPage MainPage;
        public static SearchPage SearchPage;
        public static ContactPage ContactPage;

        public static void Init(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            MainPage = new MainPage(driver);
            SearchPage = new SearchPage(driver);
            ContactPage = new ContactPage(driver);
        }
    }

}