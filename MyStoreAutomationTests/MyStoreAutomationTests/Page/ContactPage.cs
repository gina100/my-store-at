﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace MyStoreAutomationTests.Page
{
    public class ContactPage : BasePage
    {
        public ContactPage(IWebDriver webdriver) : base(webdriver)
        {

        }

        public IWebElement contactBox => PageRoot.FindElement(By.CssSelector("#center_column form"));

        private IWebElement subjectHeadingSelect => contactBox.FindElement(By.Id("id_contact"));

        public IWebElement emailInput => contactBox.FindElement(By.Id("email"));

        public IWebElement orderInput => contactBox.FindElement(By.Id("id_order"));

        public IWebElement submitButton => contactBox.FindElement(By.Id("submitMessage"));

        public IWebElement messageInput => contactBox.FindElement(By.Id("message"));

        public IWebElement successMessage => PageRoot.FindElement(By.CssSelector("#center_column > p.alert"));

        public IWebElement failureMessage => PageRoot.FindElement(By.CssSelector("#center_column > div.alert"));

        public SelectElement subjectHeadingDropDown => new SelectElement(subjectHeadingSelect);
    }
}
