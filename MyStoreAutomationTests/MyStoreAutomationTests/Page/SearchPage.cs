﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace MyStoreAutomationTests.Page
{
    public class SearchPage : MainPage
    {
        public SearchPage(IWebDriver webdriver) :base(webdriver)
        {

        }

        public IWebElement SearchRoot => PageRoot.FindElement(By.CssSelector("div.columns-container"));

        public IList<IWebElement> SearchedItemNames => SearchRoot.FindElements(By.CssSelector("li > div.product-container a.product-name"));

        public void WaitForResults()
        {
            Wait.Until(driver => SearchedItemNames.Count > 0);
        }
    }
}
