using NUnit.Framework;

namespace MyStoreAutomationTests.Tests
{
    public class SearchTests : BaseTest
    {
        [Test]
        public void BlouseSearchTest()
        {
            StoreApp.MainPage.searchInput.SendKeys("Blouse");
            StoreApp.MainPage.searchButton.Click();
            StoreApp.SearchPage.WaitForResults();
            Assert.AreEqual(StoreApp.SearchPage.SearchedItemNames[0].Text, "Blouse");
        }
    }
}