﻿using NUnit.Framework;

namespace MyStoreAutomationTests.Tests
{
    public class ContactUsTests : BaseTest
    {
        [Test]
        public void SendMessage()
        {
            StoreApp.MainPage.contactUsButton.Click();
            StoreApp.ContactPage.subjectHeadingDropDown.SelectByText("Customer service");
            StoreApp.ContactPage.orderInput.SendKeys("test");
            StoreApp.ContactPage.emailInput.SendKeys("test@test.com");
            StoreApp.ContactPage.messageInput.SendKeys("test");
            StoreApp.ContactPage.submitButton.Click();
            Assert.IsTrue(StoreApp.ContactPage.successMessage.Displayed);
        }

        [Test]
        public void ValidtionOnMessage()
        {
            StoreApp.MainPage.contactUsButton.Click();
            StoreApp.ContactPage.submitButton.Click();
            Assert.IsTrue(StoreApp.ContactPage.failureMessage.Displayed);
        }

    }
}
